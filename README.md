# Gestore Libretti Universitari #

Un semplicissimo gestore di libretti universitari scritto in Java. Per fare sì che esso funzioni basta compilare le varie classi ed eseguire main.Main.

Composto da una semplice interfaccia a linea di comando, il gestore permette di aggiungere/rimuovere esami dalla propria carriera, ottenendo velocemente statistiche su di essa quali la media ponderata, quella aritmetica, la propria base di laurea , ecc.

Il programma rende anche possibile l'inserimento di più carriere con diverse matricole (che fungono da identificatori univoci).