package model;

import java.util.*;
import java.io.*;
import java.nio.file.FileAlreadyExistsException;

public class Libretto{
	
	private int matricola;
	private File libretto;
	private TreeMap<String, Integer[]> listaEsami;
	private boolean modificato = false;
	
	//Inizio costruttore
	
	public Libretto(int matricola) throws FileAlreadyExistsException, IOException{
		
		this.matricola = matricola;
		
		File f = new File("db/voti/"+this.matricola+".l");
		listaEsami = new TreeMap<String, Integer[]>();

		if(!f.exists()){
			this.libretto = new File(f.getPath());
			boolean esito = this.libretto.createNewFile();
			
			if(!esito){
				throw new FileAlreadyExistsException(this.libretto.getPath());
			}
			
		}else{

			this.libretto = new File(f.getPath());
			
			BufferedReader br = new BufferedReader(new FileReader(this.libretto));
			String linea = br.readLine();
			StringTokenizer st;
			
			while(linea != null){
			
				st = new StringTokenizer(linea, "-");
				String nomeEsame = st.nextToken();
				String voto = st.nextToken();
				String crediti = st.nextToken();
				
				Integer[] votoCrediti = {Integer.parseInt(voto), Integer.parseInt(crediti)};
				
				listaEsami.put(nomeEsame, votoCrediti);
				
				linea = br.readLine();
			}
			
			br.close();
			
			
		}
		
	}
	
	//Fine costruttore
	
	public boolean modificato(){
		
		return modificato;
		
	}
	
	public int numeroEsamiDati(){
		
		return listaEsami.size();
		
	}
	
	public Set<String> esamiDati(){
		
		return listaEsami.keySet();

	}
	
	public ArrayList<Integer[]> votoCrediti(){
		
		return new ArrayList<Integer[]>(listaEsami.values());
		
	}
	
	public int votoEsame(String esame){
		
		return listaEsami.get(esame)[0];
		
	}
	
	public int creditiEsame(String esame){
		
		return listaEsami.get(esame)[1];
		
	}
	
	public boolean esameInCarriera(String esame){
		
		return listaEsami.containsKey(esame);
		
	}
	
	public void inserisci(String esame, int voto, int crediti){
		
		Integer[] votoCrediti = {voto, crediti};
		
		listaEsami.put(esame, votoCrediti);
		modificato = true;
		
	}
	
	public boolean elimina(String esame){
		
		Integer[] rimossoConSuccesso = listaEsami.remove(esame);
		modificato = true;

		return  rimossoConSuccesso != null;
		
	}
	
	public void salva() throws FileNotFoundException{
		
		PrintWriter pw = new PrintWriter(new File("db/voti/"+this.matricola+".l"));
		
		String[] esamiDati = new String[listaEsami.size()];
		listaEsami.keySet().toArray(esamiDati);
		
		for(int i = 0; i<esamiDati.length;i++){
			
			Integer[] votoCrediti = listaEsami.get(esamiDati[i]);
			
			if(i != esamiDati.length-1){
				pw.println(esamiDati[i]+"-"+votoCrediti[0]+"-"+votoCrediti[1]);
			}else{
				pw.print(esamiDati[i]+"-"+votoCrediti[0]+"-"+votoCrediti[1]);
			}
			
		}
		
		pw.close();
		modificato = false;
		
	}
	
	@Override
	public String toString(){
		
		String riga = String.format("%-30s%-7s%-7s"+System.lineSeparator()+System.lineSeparator(), "Esame", "Voto", "Crediti");
		
		for(String esame: listaEsami.keySet()){
			
			int voto = listaEsami.get(esame)[0];
			int crediti = listaEsami.get(esame)[1];
			
			String votoStr;

			if(voto == 31){
				votoStr = "30L";
			}else if(voto == 32){
				votoStr = "IDONEO";
			}else{
				votoStr = voto+"";
			}
			
			
			riga += String.format("%-30s%-7s%-2d"+System.lineSeparator(), esame, votoStr, crediti);
		
		}
		
		return riga;
	}
	
}