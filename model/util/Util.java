package model.util;

import model.*;

import java.text.DecimalFormat;
import java.util.*;


public final class Util{
	
	private Util(){}
	
	public static int numeroEsami(Set<String> listaEsami){
		
		return listaEsami.size();
		
	}
	
	public static int numeroCrediti(ArrayList<Integer[]> votoCrediti){
		
		int somma = 0;
		
		for(Integer[] coppia: votoCrediti){
			
			somma += coppia[1];
			
		}
		
		return somma;
		
	}
	
	public static int baseDiLaurea(String mediaPonderata){
		
		int base;
		
		try{
			base = Math.round((float)(Float.parseFloat(mediaPonderata.replace(',', '.')) *11)/3f);
		}catch(Exception e){
			base = 0;
		}
		
		return base;
		
	}
	
	public static String mediaAritmetica(ArrayList<Integer[]> votoCrediti){		
		
		int somma = 0;
		int elemento, esamiContati = 0;
		
		for(Integer[] coppia: votoCrediti){
			

			

			if(coppia[0] == 31){
				elemento = 30;
				esamiContati++;
			}else if(coppia[0] == 32){
				elemento = 0;	
			}else{
				elemento = coppia[0];
				esamiContati++;
			}
			

			
			somma += elemento;
			
		}
	    DecimalFormat f = new DecimalFormat("##.00");
		return f.format(((float)somma)/((float)esamiContati));
		
	}
	
	public static String mediaPesata(ArrayList<Integer[]> votoCrediti){
				
		int somma = 0;
		int sommaPesi = 0;
		
		for(Integer[] coppia: votoCrediti){
			
			int elemento, peso;
			
			
			if(coppia[0] == 31){
				elemento = 30;
				peso = coppia[1];
			}else if(coppia[0] == 32){
				elemento = 0;
				peso = 0;
			}else{
				elemento = coppia[0];
				peso = coppia[1];
			}
			

			
			somma += elemento*peso;
			sommaPesi += peso;
			
		}
	    DecimalFormat f = new DecimalFormat("##.00");
		return f.format(((float)somma)/((float)sommaPesi));
		
	}
	
	public static void stampa(Libretto libretto){
		
		String mediaPonderata = Util.mediaPesata(libretto.votoCrediti());

		System.out.println("Numero Esami: "+Util.numeroEsami(libretto.esamiDati()));
		System.out.println("Crediti: "+Util.numeroCrediti(libretto.votoCrediti())+"/180");
		System.out.println("Base di Laurea: "+Util.baseDiLaurea(mediaPonderata));
		System.out.println("Media Aritmetica: "+Util.mediaAritmetica(libretto.votoCrediti()));
		System.out.println("Media Ponderata: "+mediaPonderata);
		System.out.println(System.lineSeparator());

		System.out.print(libretto);
		
	}
}