package model;

public class Persona{
	
	private String nome, cognome;
	private int matricola;
	
	public Persona(String nome, String cognome, int matricola){
		
		this.nome = nome;
		this.cognome = cognome;
		this.matricola = matricola;
		
	}
	
	//Getter
	
	public String getNome(){
		
		return nome;
		
	}
	
	public String getCognome(){
		
		return cognome;
		
	}
	
	public int getMatricola(){
		
		return matricola;
		
	}
	
}