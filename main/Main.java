package main;

import model.*;
import model.util.*;

import java.io.*;
import java.util.*;
import java.util.regex.*;


public class Main{

	
	public static void main(String...args) throws FileNotFoundException, IOException{
		
		File fileElencoStudenti = new File("db/studenti/elenco.txt");
		if(!fileElencoStudenti.exists()){
			File db = new File("db");
			File dbStudenti = new File("db/studenti");
			File dbVoti = new File("db/voti");
			
			try{
				db.mkdir();
				dbStudenti.mkdir();
				dbVoti.mkdir();
				fileElencoStudenti.createNewFile();

			}catch(Exception e){
				System.out.println("Errore nella creazione di alcune cartelle per contenere i dati!");
				System.exit(-1);
			}
		}
		
		Persona studente;
		Libretto libretto;
		
		BufferedReader br = new BufferedReader(new FileReader(fileElencoStudenti));
		HashMap<Integer, String[]> elencoStudenti = new HashMap<Integer, String[]>();
		
		String linea = br.readLine();
		StringTokenizer st;
		
		while(linea != null){
			st = new StringTokenizer(linea);
			int matricola = Integer.parseInt(st.nextToken());
					
			String[] informazioni = {st.nextToken(), st.nextToken()};
			
			elencoStudenti.put(matricola, informazioni);
			
			linea = br.readLine();
			
		}

		
		Scanner sc = new Scanner(System.in);
		
		while(true){
			
			
			System.out.println("Ciao, inserisci la tua matricola! (Inserisci 0 per uscire)");
			int matricola = 0;
			boolean nonValido = true;
			
			while(nonValido){

				String matricolaStr = sc.nextLine();
				Pattern p = Pattern.compile("[0-9]+");
				Matcher m = p.matcher(matricolaStr);

				if(m.matches()){
					matricola = Integer.parseInt(matricolaStr);
					nonValido = false;
				}else{
					System.out.println("Inserisci una matricola valida (solo numeri)!");
				}
			}
			
			if(matricola == 0){
				System.out.println("Ciao, alla prossima!");
				break;
			}
			
			if(elencoStudenti.keySet().contains(matricola)){
				
				String[] nomeCognome = elencoStudenti.get(matricola);
				
				studente = new Persona(nomeCognome[0], nomeCognome[1], matricola);

				System.out.println(System.lineSeparator()+"Benvenuto/a "+ studente.getNome()+"!"+System.lineSeparator());
				
				libretto = new Libretto(studente.getMatricola());

				Util.stampa(libretto);
				
				System.out.println(System.lineSeparator());
				
				System.out.print(">:");

				

			}else{
				
				System.out.println("Ciao, per registrarti inserisci il tuo nome e cognome!");
				String nome = null, cognome = null;
				
				nonValido = true;
				while(nonValido){

					System.out.println("Nome: ");
					nome = sc.nextLine();
					Pattern p = Pattern.compile("[a-zA-Z]+");
					Matcher m = p.matcher(nome);
					
					if(m.matches()){
						nonValido = false;
					}else{
						System.out.println("Inserisci un nome valido (solo lettere)!");
					}
					
				}
				
				nonValido = true;
				while(nonValido){

					System.out.println("Cognome: ");
					cognome = sc.nextLine();
					Pattern p = Pattern.compile("[a-zA-Z]+");
					Matcher m = p.matcher(cognome);
					
					if(m.matches()){
						nonValido = false;
					}else{
						System.out.println("Inserisci un cognome valido (solo lettere)!");
					}
					
				}
				
				
				
				studente = new Persona(nome, cognome, matricola);
				libretto = new Libretto(matricola);
				
				FileWriter fw = new FileWriter(fileElencoStudenti, true);
				fw.write(matricola+"	"+nome+"	"+cognome+System.lineSeparator());
				fw.close();
			
				
				System.out.println("Benvenuto/a "+ nome+"!"+System.lineSeparator());
				System.out.print(">:");
			}
			
			while(true){
				
				char operazione; 
				try{
					operazione = sc.nextLine().toLowerCase().charAt(0);
				}catch(Exception e){
					System.out.print(">:");
					continue;
				}
				
				
				if(operazione == 'a'){
					String nomeEsame = null, voto = null, crediti = null;
					
					nonValido = true;
					while(nonValido){

						System.out.println("Nome esame: ");
						nomeEsame = sc.nextLine();
						Pattern p = Pattern.compile("[a-zA-Z0-9 ]+");
						Matcher m = p.matcher(nomeEsame);
						
						if(m.matches()){
							nonValido = false;
						}else{
							System.out.println("Inserisci un nome valido (solo lettere e numeri)!");
						}
						
					}
					
					nonValido = true;
					while(nonValido){

						System.out.println("Voto: ");
						voto = sc.nextLine();
						if(voto.equals("I")){
							voto = "32";
						}else if(voto.equals("30L")){
							voto = "31";
						}
						Pattern p = Pattern.compile("([0-9]{1,2})");
						Matcher m = p.matcher(voto);
						
						if(m.matches()){			
							nonValido = false;
						}else{
							System.out.println("Inserisci un voto valido (solo numeri)!");
						}
						
					}
					
					nonValido = true;
					while(nonValido){

						System.out.println("Crediti: ");
						crediti = sc.nextLine();
						Pattern p = Pattern.compile("[0-9]+");
						Matcher m = p.matcher(crediti);
						
						if(m.matches()){
							nonValido = false;
						}else{
							System.out.println("Inserisci un numero di crediti valido (solo numeri)!");
						}
						
					}
					
					libretto.inserisci(nomeEsame, Integer.parseInt(voto), Integer.parseInt(crediti));
					System.out.println("Esame aggiunto con successo!");
					
				}else if(operazione == 'r'){
					
					String esameDaRimuovere = null;
					
					nonValido = true;
					while(nonValido){

						System.out.println("Nome: ");
						esameDaRimuovere = sc.nextLine();
						Pattern p = Pattern.compile("[a-zA-Z ]+");
						Matcher m = p.matcher(esameDaRimuovere);
						
						if(m.matches()){
							nonValido = false;
						}else{
							System.out.println("Inserisci un nome valido (solo lettere)!");
						}
						
					}
					
					if(!libretto.elimina(esameDaRimuovere)){
						System.out.println("Nessun esame con questo nome!");
					}else{
						System.out.println("Esame eliminato con successo!");
					}
					
				}else if(operazione == 'v'){
					Util.stampa(libretto);
				}else if(operazione == 's'){
					if(libretto.modificato()){
						try{
							libretto.salva();
						}catch(Exception e){
							System.out.println("Errore nella fase di salvataggio!");
						}
						System.out.println("Modifiche salvate con successo!");
					}else{
						System.out.println("Non c'� alcuna modifica in attesa di salvataggio!");
					}
				}else if(operazione == 'c'){
					if(libretto.modificato()){
						System.out.println("Ci sono modifiche non salvate, vuoi salvare prima di uscire? S/N");
						boolean salvataggioInCorso = true;
						
						while(salvataggioInCorso){
							
							char vuoiSalvare = sc.nextLine().toLowerCase().charAt(0);
							
							if(vuoiSalvare == 's'){
								try{
									libretto.salva();
								}catch(Exception e){
									System.out.println("Errore nella fase di salvataggio!");
								}finally{
									salvataggioInCorso = false;
								}
							}else if(vuoiSalvare == 'n'){
								salvataggioInCorso = false;
							}else{
								System.out.println("Comando non valido, scegli S/N!");
							}
							
						}
					}
					System.out.println("Ciao "+studente.getNome()+"!");
					break;
				}else if(operazione == 'e'){
					if(libretto.modificato()){
						System.out.println("Ci sono modifiche non salvate, vuoi salvare prima di uscire? S/N");
						boolean salvataggioInCorso = true;
						
						while(salvataggioInCorso){
							
							char vuoiSalvare = sc.nextLine().toLowerCase().charAt(0);
							
							if(vuoiSalvare == 's'){
								try{
									libretto.salva();
								}catch(Exception e){
									System.out.println("Errore nella fase di salvataggio!");
								}finally{
									salvataggioInCorso = false;
								}
							}else if(vuoiSalvare == 'n'){
								salvataggioInCorso = false;
							}else{
								System.out.println("Comando non valido, scegli S/N!");
							}
							
						}
					}
					System.out.println("Ciao, alla prossima!");
					System.exit(-1);
				}else if(operazione == 'h'){
					System.out.println("(A)ggiungi esame(per modificarne uno esistente basta ri-aggiungerlo);\n(R)imuovi esame;\n(V)isualizza libretto;\n(S)alva modifiche;\n(C)ambia utente;\n(E)sci;\n(H)elp, lista comandi.");
				}else{
					System.out.println("Comando non valido! Premi H per una lista dei comandi!");
				}
				
				System.out.println(System.lineSeparator());
				System.out.print(">:");

				
			}
			
			
		}
		
		sc.close();
		br.close();
	}
	
}